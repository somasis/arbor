# Copyright 2008, 2009 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=gdraheim tag=v${PV} ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A library to read zip files"
DESCRIPTION="
The implementation is based only on the (free) subset of compression with the zlib algorithm.
"

LICENCES="|| ( LGPL-2 MPL-1.1 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="sdl"

# Testdata is fetched from various github locations during src_test
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*[<3]
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        sdl? ( media-libs/SDL:0 )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/37bd531377a5fe7a001598a391e368455d861b83.patch
    "${FILES}"/9411bde3e4a70a81ff3ffd256b71927b2d90dcbb.patch
    "${FILES}"/d2e5d5c53212e54a97ad64b793a4389193fec687.patch
    "${FILES}"/0e1dadb05c1473b9df2d7b8f298dab801778ef99.patch
    "${FILES}"/81dfa6b3e08f6934885ba5c98939587d6850d08e.patch
    "${FILES}"/CVE-2018-17828.part2.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --without-asan
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( sdl )

DEFAULT_SRC_TEST_PARAMS=( -j1 )

AT_M4DIR=( m4 )

src_prepare() {
    # does not yet work with python3, last checked: 0.13.69
    export PYTHON=/usr/$(exhost --build)/bin/python2

    autotools_src_prepare
}

src_configure() {
    # do not regenerate man pages
    export ac_cv_path_XMLTO=""

    default
}

