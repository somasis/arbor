# Copyright 2011 Michael Thomas <aelmalinka@gmail.com>
# Copyright 2017 Arnaud Lefebvre <a.lefebvre@outlook.fr>
# Distributed under the terms of the GNU General Public License v2

MY_PV="v${PV}"
MY_PNV="${PN}-${MY_PV}"
WORK="${WORKBASE}/${MY_PNV}"

export_exlib_phases src_configure src_install src_test

DESCRIPTION="
Designed to provide an easy way to build scalable network applications, similar to Event Machine or
Twisted.
"
HOMEPAGE="https://nodejs.org"
DOWNLOADS="${HOMEPAGE}/dist/${MY_PV}/${MY_PNV}.tar.xz"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    gdb [[ description = [ Enable GDB JIT interface for v8 ] ]]
    bundled-openssl [[
        description = [ Build node against the bundled OpenSSL instead of the system installed one ]
    ]]
"
# TODO(japaric) re-add libressl provider when libressl catches up with the new API in openssl 1.0.2
# OR when node officially supports libressl cf. https://github.com/nodejs/node/issues/428
# context: libressl is on API parity with openssl 1.0.1, but node uses the new API available in
# openssl 1.0.2 cf. https://github.com/nodejs/node/issues/2783
#   ( providers: libressl openssl  ) [[ number-selected = exactly-one  ]]

RESTRICT="test"

# See TODO above
#        providers:libressl? ( dev-libs/libressl )
#        providers:openssl? ( dev-libs/openssl  )
# TODO: bundled app-arch/brotli[>=1.0.7]
DEPENDENCIES="
    build:
        dev-lang/python:2.7
    build+run:
        dev-libs/icu:=[>=64.2]
        dev-libs/libuv[>=1.33.1]
        net-dns/c-ares[>=1.15.0]
        net-libs/nghttp2[>=1.39.2]
        sys-libs/zlib[>=1.2.11]
        !bundled-openssl? ( dev-libs/openssl[>=1.1.1c] )
"

if ever at_least 13.2.0; then
    DEPENDENCIES+="
        build:
            dev-lang/python:*
        build+run:
            net-libs/nghttp2[>=1.40.0]
    "
else
    DEPENDENCIES+="
        build:
            dev-lang/python:2.7
        build+run:
            net-libs/http-parser[>=2.8.0]
    "
fi

DEFAULT_SRC_COMPILE_PARAMS=( V=1 LD="${CXX}" )

PATH="${TMPDIR}:${PATH}"

node_src_configure() {
    local myconf=(
        --prefix=/usr/$(exhost --target)
        --openssl-use-def-ca-store
        --shared-cares
        --shared-libuv
        --shared-nghttp2
        --shared-zlib
        --with-intl=system-icu
        $(option gdb && echo --gdb)
        $(option !bundled-openssl && echo --shared-openssl)
    )

    if ! ever at_least 13.2.0; then
        myconf+=( --shared-http-parser )
    fi

    edo ./configure "${myconf[@]}"
}

node_src_install() {
    default

    # man files for node
    doman "${IMAGE}"/usr/$(exhost --target)/share/man/man1/*
    # man files for npm cli
    doman "${IMAGE}"/usr/$(exhost --target)/lib/node_modules/npm/man/man{1,5,7}/*.[157]

    edo mkdir -p "${IMAGE}"/usr/share/doc/"${PNVR}"/
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/doc/${PN}/* "${IMAGE}"/usr/share/doc/"${PNVR}"/
    edo mv "${IMAGE}"/usr/{$(exhost --target)/,}share/systemtap
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/{share/{doc/{${PN}/,},},lib/node_modules/npm/man}
}

node_src_test() {
    # rebuild certificates
    emake -j1 -C test/fixtures/keys clean
    emake -j1 -C test/fixtures/keys

    esandbox allow_net "inet:0.0.0.0@0"
    esandbox allow_net "inet:0.0.0.0@12346"
    esandbox allow_net "unix:${WORK}/test/tmp/test.sock"

    emake test
}

